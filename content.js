// let owlObj = function (navShow) {
//   return {
//     responsive: {
//       0: { items: 1 },
//       600: { items: 1 },
//       1000: { items: 1 }
//     },
//     lazyLoad: true,
//     dots: false,
//     loop: true,
//     nav: navShow,
//     navText: [
//       '<div class="menu-slider-buttons"><img src="426.png"></div>',
//       '<div class="menu-slider-buttons"><img src="424.png"></div>'],
//     autoplay: true,
//     autoplayTimeout: 3000,
//     autoplayHoverPause: false,
//   }
// }

$('.datepicker').datepicker({
  format: {
    toDisplay: function (date) {
      var d = new Date(date);
      return `${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}`;
    },
    toValue: function (date) {
      return new Date(date);
    }
  },
  autoclose: true,
  language: 'ru'
});

$(document).ready(function () {
  let carousel = $(".individual-slider");
  let wedding = $(".wedding-slider");
  let orgCarousel = $(".organization-carousel");
  let luxuryCarousel = $(".luxury-carousel");
  let platformCarousel = $(".platform-carousel");

  carousel.owlCarousel(owlObj(true));
  wedding.owlCarousel(owlObj(true));
  orgCarousel.owlCarousel(owlObj(false));
  luxuryCarousel.owlCarousel(owlObj(false));
  platformCarousel.owlCarousel(owlObj(false));

  $('#more-photos-btn-slider-left').click(function () {
    orgCarousel.trigger('prev.owl.carousel');
  })

  $('#more-photos-btn-slider-right').click(function () {
    orgCarousel.trigger('next.owl.carousel');
  })

  $('#luxury-btn-slider-left').click(function () {
    luxuryCarousel.trigger('prev.owl.carousel');
  })

  $('#luxury-btn-slider-right').click(function () {
    luxuryCarousel.trigger('next.owl.carousel');
  })

  $('#platform-btn-slider-left').click(function () {
    platformCarousel.trigger('prev.owl.carousel');
  })

  $('#platform-btn-slider-right').click(function () {
    platformCarousel.trigger('next.owl.carousel');
  })
});