$(document).ready(function () {

    let pageInfo = $(".inner-page-slider");
    pageInfo.owlCarousel({
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      },
      lazyLoad: true,
      dots: false,
      loop: true,
      nav: true,
      navText: [
        '<div class="menu-slider-buttons"><img src="426.png"></div>',
        '<div class="menu-slider-buttons"><img src="424.png"></div>'],
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: false,
    });
    // $('#cottages-1-btn-slider-left').click(function () {
    //   cottage1.trigger('prev.owl.carousel');
    // })
    // $('#cottages-1-btn-slider-right').click(function () {
    //   cottage1.trigger('next.owl.carousel');
    // })
  })