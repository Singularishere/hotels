$(document).ready(function () {
    let salesSlider = $(".sales-slider");
    let salesSliderCounter = $('.sales-slider-counter');
    salesSlider.owlCarousel({
      responsive: {
        0: { items: 1 },
        600: { items: 2 },
        1000: { items: 3 }
      },
      dots: true,
      loop: true,
      nav: true,
      navText: [
        '<div class="mr-5"><img src="icons/main-slider-arrow-left.png"></div>',
        '<div class="ml-5"><img src="icons/main-slider-arrow-right.png"></div>'],
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: false,
      center: true,
      onInitialized: counter,
      onTranslated: counter
    });
  
    function counter(event) {
      let items = event.item.count
      let item = event.page.index + 1;
      salesSliderCounter.html(item + "/" + items)
    }
  })