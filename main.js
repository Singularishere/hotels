let months = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

let owlObj = function (navShow) {
    return {
        responsive: {
            0: { items: 1 },
            600: { items: 1 },
            1000: { items: 1 }
        },
        lazyLoad: true,
        dots: false,
        loop: true,
        nav: navShow,
        navText: [
            '<div class="menu-slider-buttons"><img src="426.png"></div>',
            '<div class="menu-slider-buttons"><img src="424.png"></div>'],
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: false,
    }
}

$(document).ready(() => {
    let salesSlider = $(".sales-slider");
    let videoSlider = $(".video-slider");
    let photoSlider = $(".photo-slider");
    let salesSliderCounter = $('.sales-slider-counter');
    let headerBlock = $('.img-responsive');
    let menuDesktop = $('#menu-desktop');
    let menuMobile = $('#menu-mobile');
    let menuOpen = false;

    menuDesktop.hide();
    menuMobile.hide();

    document.addEventListener('wheel', fn, { passive: false });
    document.addEventListener('mousewheel', fn, { passive: false });
    document.addEventListener('DOMMouseScroll', fn, { passive: false });

    function fn(event) {
        if (menuOpen) event.preventDefault()
    }

    $('#menu-button').click(() => menuToggle(true));
    $('#menu-button-close').click(() => menuToggle(false));
    $('#menu-button-desktop').click(() => menuToggle(false));
    $('#mobile-menu-button').click(() => menuToggle(false));

    salesSlider.owlCarousel({
        responsive: {
            0: { items: 1 },
            600: { items: 2 },
            1000: { items: 3 }
        },
        dots: true,
        loop: true,
        nav: true,
        navText: [
            '<div class="mr-5"><img src="icons/main-slider-arrow-left.png"></div>',
            '<div class="ml-5"><img src="icons/main-slider-arrow-right.png"></div>'],
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: false,
        center: true,
        onInitialized: counter,
        onTranslated: counter
    });

    videoSlider.owlCarousel({
        items: 2,
        responsive: {
            0: { items: 1 },
            600: { items: 1 },
            1000: { items: 2 }
        },
        merge: true,
        margin: 0,
        dots: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: false,
        video: true,
        videoWidth: 525,
        videoHeight: 500
    });

    $('.video-arrow-left').click(function () {
        videoSlider.trigger('prev.owl.carousel');
    })

    $('.video-arrow-right').click(function () {
        videoSlider.trigger('next.owl.carousel');
    })

    photoSlider.owlCarousel({
        items: 1,
        responsive: {
            0: { items: 1 },
            600: { items: 1 },
            1000: { items: 1 }
        },
        dots: true,
        loop: true,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause: false
    });

    $('.photo-arrow-left').click(function () {
        photoSlider.trigger('prev.owl.carousel');
    })

    $('.photo-arrow-right').click(function () {
        photoSlider.trigger('next.owl.carousel');
    })

    function counter(event) {
        let items = event.item.count
        let item = event.page.index + 1;
        salesSliderCounter.html(item + "/" + items)
    }

    function menuToggle(show) {
        if (screen.width >= 768) {
            show ? menuDesktop.show() : menuDesktop.hide();
            menuOpen = show;
        } else {
            if (show) {
                menuMobile.show();
                headerBlock.hide();
            } else {
                menuMobile.hide();
                headerBlock.show();
            }
            menuOpen = false;
        }
    }

    $('.datepicker').datepicker({
        format: {
            toDisplay: function (date) {
                var d = new Date(date);
                return `${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}`;
            },
            toValue: function (date) {
                return new Date(date);
            }
        },
        autoclose: true,
        language: 'ru'
    });
});