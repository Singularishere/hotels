$('.datepicker').datepicker({
  format: {
    toDisplay: function (date) {
      var d = new Date(date);
      return `${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}`;
    },
    toValue: function (date) {
      return new Date(date);
    }
  },
  autoclose: true,
  language: 'ru'
});
$(document).ready(function () {
  let lux1 = $(".lux-carousel-1");
  lux1.owlCarousel({
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    },
    lazyLoad: true,
    dots: false,
    loop: true,
    nav: false,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: false,
  });
  $('#lux-1-btn-slider-left').click(function () {
    lux1.trigger('prev.owl.carousel');
  })
  $('#lux-1-btn-slider-right').click(function () {
    lux1.trigger('next.owl.carousel');
  })
  let cottage1 = $(".cottages-carousel-1");
  cottage1.owlCarousel({
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    },
    lazyLoad: true,
    dots: false,
    loop: true,
    nav: false,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: false,
  });
  $('#cottages-1-btn-slider-left').click(function () {
    cottage1.trigger('prev.owl.carousel');
  })
  $('#cottages-1-btn-slider-right').click(function () {
    cottage1.trigger('next.owl.carousel');
  })
  let salesSlider = $(".sales-slider");
  let salesSliderCounter = $('.sales-slider-counter');
  salesSlider.owlCarousel({
    responsive: {
      0: { items: 1 },
      600: { items: 2 },
      1000: { items: 3 }
    },
    dots: true,
    loop: true,
    nav: true,
    navText: [
      '<div class="mr-5"><img src="icons/main-slider-arrow-left.png"></div>',
      '<div class="ml-5"><img src="icons/main-slider-arrow-right.png"></div>'],
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: false,
    center: true,
    onInitialized: counter,
    onTranslated: counter
  });

  function counter(event) {
    let items = event.item.count
    let item = event.page.index + 1;
    salesSliderCounter.html(item + "/" + items)
  }
})